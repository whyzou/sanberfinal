@extends('layouts.master')
@section('title')
    Pertanyaan {{ $kategori->nama }}
@endsection

@section('content')
    <div class="row">
        @forelse ($pertanyaan as $key => $item)
            <div class="col-3">
                <div class="card">
                    <img style="max-height: 200px; object-fit:cover" src="{{ asset('images/' . $item->gambar) }}"
                        class="card-img-top" alt="Pertanyaan {{ $item->id }}">
                    <div class="card-body">
                        <h5 class="card-title font-weight-bold">{{ $item->judul }}</h5>
                        <p class="card-text">{!! Str::limit($item->isi_pertanyaan, 50) !!}</p>
                        <a href="/pertanyaan/{{ $item->id }}" class="btn btn-info btn-block btn-sm">Detail</a>
                        @if (Auth::user()->id == $item->user_id)
                            <div class="row my-2">
                                <div class="col">
                                    <a href="/pertanyaan/{{ $item->id }}/edit"
                                        class="btn btn-success btn-block btn-sm">Update</a>
                                </div>
                                <div class="col">
                                    <form action="/kategori/{{ $item->id }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                                    </form>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @empty
            <div class="alert alert-danger" role="alert">
                Tidak ada Pertanyaan
            </div>
        @endforelse
    </div>
@endsection
