@extends('layouts.master')
@section('title')
    List Kategori Pertanyaan
@endsection
@push('datatables')
    <script>
        $(function() {
            $("#dt-kategori").DataTable();
        });
    </script>
@endpush
@section('content')
    <a class="btn btn-primary mb-3" href="/kategori/create" role="button">Add Kategori</a>
    <table class="table table-striped table-bordered text-center" id="dt-kategori">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($kategori as $key =>$item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>
                        <form action="/kategori/{{ $item->id }}" method="post">
                            <a href="/kategori/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/kategori/{{ $item->id }}/edit" class="btn btn-success btn-sm">Update</a>
                            @method('delete')
                            @csrf
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                </tr>

            @empty
            @endforelse

        </tbody>
    </table>
    @include('sweetalert::alert')
@endsection
