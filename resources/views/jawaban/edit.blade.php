@extends('layouts.master')
@section('title')
    Edit Jawaban
@endsection

@section('content')
    <form action="/jawaban/{{ $jawaban->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Isi Jawaban</label>
            <textarea name="isi_jawaban" class="form-control" rows="2">{{ $jawaban->isi_jawaban }}</textarea>
        </div>
        @error('isi_jawaban')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="gambar">Gambar</label>
            <input type="file" class="form-control-file" name="gambar" id="gambar">
        </div>
        @error('gambar')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    @push('script')
    <script src="https://cdn.tiny.cloud/1/wqoikrq3gze3v1292wi3jfnj9f558a303tmaeq2gkwevm1cp/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
          selector: 'textarea',
          plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
          toolbar_mode: 'floating',
        });
      </script>
    @endpush
@endsection
