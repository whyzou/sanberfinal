@extends('layouts.master')
@section('title')
    Create Pertanyaan
@endsection
@push('select2')
    <script>
        $('.select2').select2({
            placeholder: '-- Pilih Kategori --',
            width: '100%'
        });
    </script>
@endpush

@section('content')
    <form action="/pertanyaan" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" class="form-control" name="judul" id="judul">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="kategori_id">Kategori</label>
            <select name="kategori_id" id="kategori_id" class="form-control js-states select2">
                <option value=""></option>
                @forelse ($kategori as $item)
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @empty
                    <option value="">Kategori Kosong</option>
                @endforelse
            </select>
        </div>
        @error('kategori_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="isi_pertanyaan">Isi Pertanyaan</label>
            <textarea name="isi_pertanyaan" id="isi_pertanyaan" class="form-control" rows="3"></textarea>
        </div>
        @error('isi_pertanyaan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="gambar">Gambar</label>
            <input type="file" class="form-control-file" name="gambar" id="gambar">
        </div>
        @error('gambar')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@push('script')
<script src="https://cdn.tiny.cloud/1/wqoikrq3gze3v1292wi3jfnj9f558a303tmaeq2gkwevm1cp/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      toolbar_mode: 'floating',
    });
  </script>
@endpush
@endsection
