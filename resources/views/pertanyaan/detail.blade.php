@extends('layouts.master')
@section('title')
    Halaman Detail Pertanyaan
@endsection

@section('content')
    <div class="card mb-3">

        <img style="max-height: 400px; object-fit:cover" class="card-img-top"
            src="{{ asset('images/' . $pertanyaan->gambar) }}" alt="Pertanyaan {{ $pertanyaan->id }}">
        <div class="card-body">
            <h5 class="card-title font-weight-bold">{{ $pertanyaan->judul }}</h5>
            <p class="card-text text-justify">{!! $pertanyaan->isi_pertanyaan !!}</p>
            <p class="card-text"><small class="text-muted">By:
                    {{ $pertanyaan->user->nama . ' (' . $pertanyaan->tanggal . ')' }}</small>
            </p>
        </div>
    </div>
    <hr>
    <h4>List Jawaban</h4>
    <hr>
    @forelse ($jawaban as $item)
        <div class="media my-3 p-3 callout callout-info">
            <img class="mx-auto img-thumbnail" src="{{ asset('images/profile.png') }}"
                style="border-radius: 50%; max-width:70px">
            <div class="media-body ml-3">
                <label>
                    <strong class="text-lg">{{ $item->user->nama }}</strong> <small
                        class="font-italic">({{ $item->tanggal }})</small>
                </label>

                <br>
                @if ($item->gambar != null)
                    <img style="max-height: 200px; max-width:500px" class=""
                        src="{{ asset('images/' . $item->gambar) }}" alt="">
                @endif
                <p>{!! $item->isi_jawaban !!}</p>

            </div>
            @if (Auth::user()->id == $item->user_id)
                <a href="/jawaban/{{ $item->id }}/edit" class="btn btn-success btn-sm mx-1">Update</a>
                <form action="/jawaban/{{ $item->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                </form>
            @endif
        </div>
    @empty
        <div class="alert alert-danger" role="alert">
            Tidak ada Jawaban
        </div>
    @endforelse
    <!-- general form elements -->
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">Form Jawaban</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="/jawaban" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <textarea name="isi_jawaban" cols="30" class="form-control" placeholder="Isikan Jawaban" rows="2"></textarea>
                    @error('isi_jawaban')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <div class="form-group">
                        <input type="file" class="form-control-file" name="gambar" id="gambar">
                    </div>
                    @error('gambar')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <input type="hidden" name="pertanyaan_id" value="{{ $pertanyaan->id }}">
                <input type="submit" value="Answer!" class="btn btn-success col-2">
            </div>
            <!-- /.card-body -->
        </form>
    </div>
    <!-- /.card -->
    {{-- <a class="btn btn-danger mb-3 btn-block" href="/pertanyaan/create" role="button">Kembali</a> --}}
    @push('script')
        <script src="https://cdn.tiny.cloud/1/wqoikrq3gze3v1292wi3jfnj9f558a303tmaeq2gkwevm1cp/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
        <script>
            tinymce.init({
                selector: 'textarea',
                plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                toolbar_mode: 'floating',
            });
        </script>
    @endpush
@endsection
