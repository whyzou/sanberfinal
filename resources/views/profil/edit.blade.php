@extends('layouts.master')
@section('title')
    Edit Profile
@endsection

@section('content')
    <form action="/profil/{{ $profil->id }}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="nama" class="form-control" name="nama" value="{{ $profil->user->nama }}">
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" name="email" value="{{ $profil->user->email }}" disabled>
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="umur" class="form-control" name="umur" value="{{ $profil->umur }}">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" class="form-control" rows="3">{{ $profil->alamat }}</textarea>
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Biodata</label>
            <textarea name="bio" class="form-control" rows="3">{{ $profil->bio }}"</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    
@endsection
