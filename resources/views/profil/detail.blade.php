@extends('layouts.master')
@section('title')
    Detail Profile
@endsection

@section('content')
    <!-- Profile Image -->
    <div class="card card-primary card-outline">
        <div class="card-body box-profile">
            <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="{{ asset('images/profile.png') }}"
                    alt="User profile picture">
            </div>
            <h3 class="profile-username text-center">{{ $profil->user->nama }}</h3>
            <p class="text-muted text-center">{{ $profil->user->email }}</p>
            <a href="/profil/{{ Auth::id() }}/edit" class="btn btn-success btn-block"><b>Edit Profil</b></a>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

    <!-- About Me Box -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">About Me</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <strong><i class="fas fa-book mr-1"></i> Umur</strong>
            <p class="text-muted">
                {{ $profil->umur }}
            </p>
            <hr>
            <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>
            <p class="text-muted">{{ $profil->alamat }}</p>
            <hr>
            <strong><i class="far fa-file-alt mr-1"></i> Biodata</strong>
            <p class="text-muted">{{ $profil->bio }}</p>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
    </div>
    @include('sweetalert::alert')
@endsection
