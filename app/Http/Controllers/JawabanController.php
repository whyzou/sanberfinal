<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jawaban;
use App\Models\Kategori;
use App\Models\Pertanyaan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class JawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'isi_jawaban' => 'required',
            'gambar' => 'image|mimes:png,jpg,jpeg'
        ]);

        $jawaban = new Jawaban;

        if ($request->has('gambar')) {
            $namaFile = "image-" . uniqid() . "." . $request->gambar->extension();
            $request->gambar->move(public_path('images'), $namaFile);
            $jawaban->gambar = $namaFile;
        }

        $jawaban->isi_jawaban = $request->isi_jawaban;
        $jawaban->pertanyaan_id = $request->pertanyaan_id;
        $jawaban->tanggal = date("Y-m-d");
        $jawaban->user_id = Auth::id();
        $jawaban->save();

        Alert::success('Success', 'Berhasil Menambahkan Jawaban!');
        return redirect('/pertanyaan/' . $request->pertanyaan_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jawaban = Jawaban::find($id);
        return view('jawaban.edit', ['jawaban' => $jawaban]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'isi_jawaban' => 'required',
            'gambar' => 'image|mimes:png,jpg,jpeg'
        ]);

        $jawaban = Jawaban::find($id);

        if ($request->has('gambar')) {
            File::delete('images/' . $jawaban->gambar);

            $namaFile = "image-" . uniqid() . "." . $request->gambar->extension();
            $request->gambar->move(public_path('images'), $namaFile);
            $jawaban->gambar = $namaFile;
        }

        $jawaban->isi_jawaban = $request->isi_jawaban;
        $jawaban->save();

        Alert::success('Success', 'Berhasil Edit Jawaban!');
        return redirect('/pertanyaan/' . $jawaban->pertanyaan_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jawaban = Jawaban::find($id);
        if ($jawaban->gambar != null)
            File::delete('images/' . $jawaban->gambar);

        $jawaban->delete();

        Alert::success('Success', 'Berhasil Menghapus Jawaban!');
        return redirect('/pertanyaan/' . $jawaban->pertanyaan_id);
    }
}
