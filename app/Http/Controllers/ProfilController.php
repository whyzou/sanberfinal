<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profil;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Profiler\Profile;
use RealRashid\SweetAlert\Facades\Alert;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->first();
        $profil = Profil::where('id', $user->profil_id)->first();
        return view('profil.detail', ['profil' => $profil]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // mencari data  user berdasarkan id user
        $user = User::where('id', $id)->first();

        //setelah ketemu data usernya, cari profil berdasarkan profil_id yang didapatkan dari tabel usernya
        $profil = Profil::where('id', $user->profil_id)->first();
        return view('profil.edit', ['profil' => $profil]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'alamat' => 'required',
            'bio' => 'required',
        ]);

        //save nama di tabel user
        $user = User::find(Auth::id());
        $user->nama = $request->nama;
        $user->save();

        //save data di tabel profil
        $profil = Profil::find($id);
        $profil->umur = $request->umur;
        $profil->alamat = $request->alamat;
        $profil->bio = $request->bio;
        $profil->save();

        Alert::success('Success', 'Berhasil Edit Profile!');
        return redirect('/profil/' . Auth::id());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
