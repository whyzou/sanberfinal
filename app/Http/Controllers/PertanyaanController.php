<?php

namespace App\Http\Controllers;

use App\Models\Jawaban;
use App\Models\Kategori;
use App\Models\Pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;


class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::orderBy('tanggal', 'desc')->get();
        return view('pertanyaan.index', ['pertanyaan' => $pertanyaan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::get();
        return view('pertanyaan.create', ['kategori' => $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi_pertanyaan' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'image|mimes:png,jpg,jpeg',
        ]);

        $pertanyaan = new Pertanyaan;
        $namaFile = "default.jpg";
        if ($request->has('gambar')) {
            $namaFile = "image-" . uniqid() . "." . $request->gambar->extension();
            $request->gambar->move(public_path('images'), $namaFile);
        }
        $pertanyaan->judul = $request->judul;
        $pertanyaan->isi_pertanyaan = $request->isi_pertanyaan;
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->tanggal = date("Y-m-d");
        $pertanyaan->gambar = $namaFile;
        $pertanyaan->user_id = Auth::id();
        $pertanyaan->save();

        Alert::success('Success', 'Berhasil Menambahkan Pertanyaan!');
        return redirect('pertanyaan');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $jawaban = Jawaban::where('pertanyaan_id', $id)->orderBy('tanggal', 'asc')->get();
        return view('pertanyaan.detail', ['jawaban' => $jawaban, 'pertanyaan' => $pertanyaan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        $kategori = Kategori::get();

        return view('pertanyaan.edit', ['pertanyaan' => $pertanyaan, 'kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'isi_pertanyaan' => 'required',
            'kategori_id' => 'required',
            'gambar' => 'image|mimes:png,jpg,jpeg',
        ]);
        $pertanyaan = Pertanyaan::find($id);

        if ($request->has('gambar')) {
            if ($pertanyaan->gambar != 'default.jpg')
                File::delete('images/' . $pertanyaan->gambar);

            $namaFile = "image-" . uniqid() . "." . $request->gambar->extension();
            $request->gambar->move(public_path('images'), $namaFile);
            $pertanyaan->gambar = $namaFile;
        }

        $pertanyaan->judul = $request->judul;
        $pertanyaan->isi_pertanyaan = $request->isi_pertanyaan;
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->tanggal = date("Y-m-d");
        $pertanyaan->save();

        Alert::success('Success', 'Berhasil Edit Pertanyaan!');
        return redirect('pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        if ($pertanyaan->gambar != 'default.jpg')
            File::delete('images/' . $pertanyaan->gambar);

        $pertanyaan->delete();
        Alert::success('Success', 'Berhasil Hapus Pertanyaan!');
        return redirect('pertanyaan');
    }
}
