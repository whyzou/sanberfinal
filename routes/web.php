<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\JawabanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::middleware(['auth'])->group(function () {
    //CRUD Kategori
    Route::resource('kategori', KategoriController::class);

    //CRUD Pertanyaan
    Route::resource('pertanyaan', PertanyaanController::class);

    //CRUD Jawaban
    Route::resource('jawaban', JawabanController::class);

    // Profile
    Route::resource('profil', ProfilController::class)->only(['show', 'edit', 'update']);
});

Auth::routes();
